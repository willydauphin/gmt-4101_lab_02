package exercice_1_2;
public class Liste {
	private Object elemTete ;
	private Liste queue ;
	
	/**
	 * EXERCICE 01
	 */
	
	/**
	 * Le constructeur qui cr�e une liste vide
	 * <p>
	 * Les deux attributs sont initialis�s � null.
	 * 
	 * @author Boffy William
	 */
	public Liste(){
		this.elemTete = null;
		this.queue = null;
	}

	/**
	 * Le constructeur qui cr�e une liste contenant comme unique �l�ment l'objet o
	 * <p>
	 * L'attribut queue est initialis� � null. L'objet o est attribu� � l'attribut elemTete.
	 * 
	 * @author Boffy William
	 * @param  o Objet unique de la liste
	 */
	public Liste(Object o){
		this.elemTete = o;
		this.queue = null;
	}

	//
	
	/**
	 * Teste si la liste est vide
	 * <p>
	 * Si l'objet n'est pas nul : renvoie false. Sinon teste sur le queue.
	 * 
	 * @author Boffy William
	 * @return boolean
	 */
	public boolean estVide() {
		boolean res = true;
		if(this.elemTete == null){
			if(this.queue != null){
				res = this.queue.estVide();
			}
		}else{
			res = false;
		}
		return res;
	}
	
	/**
	 * Retourne une liste o� l�objet o est ajout� en t�te de la liste courante
	 * <p>
	 * Ajoute � une nouvelle liste o ayant pour queue this SI liste courante vide.
	 * SINON, ajoute � la liste courante en tant que t�te o.
	 * 
	 * @author Boffy William
	 * @param  o Objet � ajouter en t�te de liste
	 * @return la liste avec o en t�te
	 */
	public Liste ajouterEnTete(Object o) {
			if(this.estVide()) {
				Liste res = this.copie();
				res.elemTete = o;
				return res;
			}else {
				Liste l = new Liste(o);
				l.queue = this;
				return l;				
			}
	} 

	/**
	 * Renvoie l'�l�ment en t�te de la liste
	 * <p>
	 * Accesseur sur elemTete
	 * 
	 * @author Boffy William
	 * @return elemTete
	 */
	public Object tete(){
		return this.elemTete;
	}
	
	/**
	 * Renvoie la queue de la liste apr�s le premier �l�ment
	 * <p>
	 * Retourne la queue de la liste compl�te
	 * 
	 * @author Boffy William
	 * @return queue
	 */
	public Liste queue() {
		if(this.queue == null) {
			return this;
		}else {
			return this.queue.queue();
		}
	}
	
	/**
	 * Renvoie l'�l�ment pr�c�dent (queue) de la liste
	 * 
	 * @author Boffy William
	 * @return queue
	 */
	public Liste precedent() {
		return this.queue;
	}
	
	/**
	 * Retourne le nombre d��l�ments dans la liste
	 * <p>
	 * Fait appel � la m�me m�thode dans la queue 
	 * 
	 * @author Boffy William
	 * @return nombre d'�l�ments de la liste
	 */
	public int longueur() {
		if(this.estVide()) {
			return 0;
		}else {
			if(this.queue == null) {
				return 1;
			}else {
				return this.queue.longueur()+1;				
			}
		}
	}

	/**
	 * Retourne une cha�ne contenant les �l�ments de la liste
	 * <p>
	 * Appel � la m�me m�thode dans queue
	 * 
	 * @author Boffy William
	 * @return retourne une liste au format "elem1 elem2 elem3 ... elemN" ou [] pour vide
	 */
	@Override
	public String toString () {
		if(this.estVide()) {
			return "[]";
		}
		if(this.queue==null){
			return this.tete().toString();
		}else {
			return this.queue + ", " + this.tete();
		}
	}
	
	/**
	 * FIN EXERCICE 01
	 */
	
	/**
	 * EXERCICE 02
	 */
	
	/**
	 * Teste si l�objet o appartient ou non � la liste
	 * <p>
	 * NE DOIT PAS MODIFIER OBJET COURANT : OK
	 * @author Boffy William
	 * @return bool�en : vrai si pr�sent dans la liste / faux sinon
	 */
	public boolean contient(Object o) {
		if(o.equals(this.elemTete)) {
			return true;
		}else if(this.queue!=null){
			return this.queue.contient(o);
		}else {
			return false;
		}
	}
	
	/**
	 * Retourne une nouvelle Liste qui ajoute l�objet o en queue de liste courante
	 * <p>
	 * Ajoute o en queue de la liste courante et renvoie la liste courante nouvelle
	 * NE DOIT PAS MODIFIER OBJET COURANT : OK
	 * @author Boffy William
	 * @return 
	 */
	public Liste ajouterEnQueue(Object o) {
		Liste res = this.copie();
		res.queue().queue = new Liste(o);
		return res;
	}
	
	/**
	 * Ajout de la liste l2 � la suite de la liste courante
	 * <p>
	 * Retourne la liste l2 avec la liste courante comme queue
	 * NE DOIT PAS MODIFIER OBJET COURANT : OK
	 * @author Boffy William
	 * @return 
	 */
	public Liste concat(Liste l2) {
		l2.queue().queue = this;
		return l2;
	}
	
	/**
	 * Retourne une liste inverse de la liste courante
	 * <p>
	 * NE DOIT PAS MODIFIER OBJET COURANT : OK
	 * 
	 * @author Boffy William
	 * @return 
	 */
	public Liste inverse() {
		Liste res = new Liste();
		Liste current = this;
		while(current!=null) {
			res = res.ajouterEnTete(current.elemTete);
			current = current.queue;
		}
		return res;
	}
	
	/**
	 * Retourne une copie de la liste courante
	 * <p>
	 * NE DOIT PAS MODIFIER OBJET COURANT : OK
	 * 
	 * @author Boffy William
	 * @return 
	 */
	public Liste copie() {
		Liste res = new Liste(this.elemTete);
		Liste current = this.queue;
		while(current!=null) {
			res = res.ajouterEnQueue(current.elemTete);
			current = current.queue;
		}
		return res;
	} 
}
