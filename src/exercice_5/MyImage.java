package exercice_5;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

public class MyImage {
	private String titre;
	private String description;
	private String path;
	private Image img;
	private String name;
	final static String[] extensions = {"jpg", "jpeg", "png"};
	
	/**
	    Constructeur d'une image.
	    @param p le chemin de l'image.
	    @param n le titre et nom de l'image.
	*/
	public MyImage(String p, String n) {
		this.titre = n;
		this.description = "Pas de description";
		this.path = p;
		this.name = n;
		try {
		    this.img = ImageIO.read(new File(this.getPath()));
		} catch (IOException e) {
			System.out.println("L'image '" + this.getPath() + "' n'a pas pu �tre charg�e");
		}
	}
	
	public String getTitre() {
		return this.titre;
	}	
	public String getName() {
		return this.name;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPath() {
		return this.path;
	}
	public Image getImg() {
		return this.img;
	}
	public void setImg(Image i) {
		this.img = i;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public static String[] getExtensions() {
		return extensions;
	}
	
	/**
	    V�rifie si l'extension d'un fichier est comprise dans la liste de valeurs accept�es (tableau constant static).
	    @param txt Le chemin.
	    @return La valeur de la somme des deux entiers sp�cifi�s.
	*/
	static boolean checkExtension(String txt) {
		String[] values = txt.split("\\.");
		String extension = values[values.length-1];
		for(int i = 0; i<MyImage.getExtensions().length; i++) {
			if(MyImage.getExtensions()[i].equals(extension)){
				return true;
			}
		}
		return false;
	}

	/**
	    Surcharge pour comparer un objet MyImage en fonction d'un autre..
	    @param that Objet � tester.
	    @return true si le nom et le chemin sont identiques.
	*/
	@Override
	public boolean equals(Object that){
		if(this == that) return true;
		if(!(that instanceof MyImage)) return false;
		MyImage imgNew = (MyImage)that;
		
		return this.name.equals(imgNew.name) && this.path.equals(imgNew.path);
	}


}
