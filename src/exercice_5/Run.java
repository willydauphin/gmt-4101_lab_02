package exercice_5;

public class Run {
	public static void main(String[] args) {
		//L'application est consid�r�e comme un album d'images.
		//Cr�ation de l'album, c'est le mod�le de donn�es.
		Album model = new Album();
		//Cr�ation d'une vue, c'est l'interface principale.
		AlbumView view = new AlbumView("Mon visualiseur d'images");
		//Cr�ation du controller, c'est ce qui unie les deux pr�c�dentes parties.
		AlbumController ctrl = new AlbumController(model, view);
		//Lancement de l'application.
		ctrl.run();
	}
}