package exercice_5;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class AlbumController {
    private Album model;
    private AlbumView view;

    /**
	    Constructeur de AlbumController
	    @param model Mod�le d'un album.
	    @param view Vue d'un album
	*/
    public AlbumController(Album model, AlbumView view){
        this.model = model;
        this.view = view;
    }
    
    /**
	    Permet d'ajouter les event-listeners (lien view-controller) en respectant MVC
	*/
    private void addListeners(){	
        this.view.addListeners(
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//ajouter un nouveau fichier
	                actionFileAdd();
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//quitter la fen�tre principale
	                actionFileExit();
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//� propos
	                actionAboutMore();
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//afficher l'image selectionn�e
	                actionPrint((String)view.getList().getSelectedValue());
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//Image suivante
	            	MyImage i = model.getItr().next();
	            	if(view.getList().getSelectedValue()!=i.getName()) {
		            	updateViewWhenChangingImage(i, model.getItr().hasNext(), model.getItr().hasPrevious());
	            	}else {
		            	updateViewWhenChangingImage(model.getItr().next(), model.getItr().hasNext(), model.getItr().hasPrevious());
	            	}
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//Image pr�c�dente
	            	MyImage i = model.getItr().previous();
	            	if(view.getList().getSelectedValue()!=i.getName()) {
		            	updateViewWhenChangingImage(i, model.getItr().hasNext(), model.getItr().hasPrevious());
	            	}else {
		            	updateViewWhenChangingImage(model.getItr().previous(), model.getItr().hasNext(), model.getItr().hasPrevious());
	            	}
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//Ouverture fen�tre modification de l'image
	            	actionModifImage();
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//Fermeture fen�tre modification de l'image
	            	view.getModifDialog().dispose();
	            	view.getModifDialog().setVisible(false);
	            }
            },
        	new ActionListener(){
	            @Override
	            public void actionPerformed(ActionEvent event)
	            {
	            	//Validation fen�tre modification de l'image
	            	MyImage current = model.getCurrent();
	            	current.setTitre(view.getTitreArea().getText());
	            	current.setDescription(view.getDescArea().getText());
	            	view.updateImage(current);
	            	view.getModifDialog().dispose();
	            	view.getModifDialog().setVisible(false);	            	
	            }
            }
        );
    }
    
    /**
	    Lancement de l'application (initialisation et affichage fen�tre principale)
	*/
    public void run() {
    	this.init();
		this.view.getFrame().setVisible(true);
    }
    
    /**
	    Initialisation � partir du mod�le de d�part et mise � jour de la vue.
	    @param a Le premier nombre entier.
	    @param b Le deuxi�me nombre entier.
	    @return La valeur de la somme des deux entiers sp�cifi�s.
	*/
    public void init() {
    	//Lien entre actions
        this.addListeners();
        
        //Pour l'ajout d'images
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Images", MyImage.extensions);
        this.view.getChooser().setFileFilter(filter);
        
        //Initialisation de l'album avec les images du r�pertoire d�fini
        File f = new File(Album.FOLDER);
    	File[] allSubFiles = f.listFiles();
        
    	//Si fichiers pr�sents
        if(allSubFiles != null) {
            for (File file : allSubFiles) {
                if(file.isFile() && MyImage.checkExtension(file.getAbsolutePath())){
                	this.addImage(new MyImage(Album.FOLDER + "\\" + file.getName(), file.getName()));
                }
            }	
        }
    }
    
    /**
	    Arr�t de l'application
	*/
    public void stop() {
    	this.view.getFrame().dispose();
    	System.exit(0);
    }
    
    /**
	    Ajout d'un fichier avec le JFileChooser()
	*/
    private void actionFileAdd(){
        int returnVal = this.view.getChooser().showOpenDialog(this.view.getFrame());
        if(returnVal == JFileChooser.APPROVE_OPTION) {
        	this.addImage(new MyImage(this.view.getChooser().getSelectedFile().getAbsolutePath(), this.view.getChooser().getSelectedFile().getName()));
        }
    }
    
    /**
	    Fermeture fen�tre principale
	*/
    private void actionFileExit(){
    	this.stop();
    } 
    
    /**
	    Affichage de la boite de dialogue "� propos".
	*/
    private void actionAboutMore(){
        if (this.view.getAboutDialog().getParent() != null) {
        	//On centre la nouvelle fen�tre dans la fen�tre principale
        	Dimension parentSize = this.view.getAboutDialog().getParent().getSize();
        	Point p = this.view.getAboutDialog().getParent().getLocation();
        	
        	//FORMULE A REVOIR
        	this.view.getAboutDialog().setLocation(p.x + parentSize.width/3, p.y + parentSize.height/3);
        }
    	this.view.getAboutDialog().setVisible(true);
    } 

    /**
	    Affichage de la boite de dialogue "modification de l'image".
	*/
    private void actionModifImage(){
        if (this.view.getModifDialog().getParent() != null) {
        	//On centre la nouvelle fen�tre dans la fen�tre principale
        	Dimension parentSize = this.view.getModifDialog().getParent().getSize();
        	Point p = this.view.getModifDialog().getParent().getLocation();
        	
        	//FORMULE A REVOIR
        	this.view.getModifDialog().setLocation(p.x + parentSize.width/3, p.y + parentSize.height/3);
        }
    	this.view.getModifDialog().setVisible(true);
    }
    
    /**
	    Affichage de l'image courante s�lectionn�e.
	*/
    private void actionPrint(String s) {
		this.model.setItr(this.model.getImages().listIterator());
    	while(this.model.getItr().hasNext()) {
    		boolean hasPrevious = this.model.getItr().hasPrevious();
			MyImage i = this.model.getItr().next();
			if(i.getName().equals(this.view.getList().getSelectedValue())) {
		        this.updateViewWhenChangingImage(i, this.model.getItr().hasNext(), hasPrevious);
				return;
			}
		}
    }
    
    /**
	    Ajout d'une image
	    @param i L'image � ajouter.
	*/
    private void addImage(MyImage i) {
    	//MAJ MODEL
    	for(MyImage img : this.model.getImages()) {
    		if(img.equals(i)){
    			System.out.println("IMAGE EXISTANTE !");
    			return;
    		}
    	}
    	this.model.addImage(i);
		this.model.setItr(this.model.getImages().listIterator());
    	while(this.model.getItr().hasNext()) {
			this.model.getItr().next();
		}
    	//MAJ VIEW
        ((DefaultListModel)this.view.getList().getModel()).addElement(i.getName());
        this.updateViewWhenChangingImage(i, false, this.model.getItr().hasPrevious());
    }

    /**
	    Mise � jour de la vue lors d'un changement d'image (gestion du suivant, pr�c�dent, etc.).
	    @param i L'image ajout�e.
	    @param hasNext Si une image suit.
	    @param hasPrevious Si une image pr�c�de.
	    @return La valeur de la somme des deux entiers sp�cifi�s.
	*/
    private void updateViewWhenChangingImage(MyImage i, boolean hasNext, boolean hasPrevious) {
        //On met � jour l'image courante de la vue
    	this.view.updateImage(i);
    	//On modifie la liste pour s�lectionner la m�me image
		this.view.getList().setSelectedValue(i.getName(), true);
		//On met � jour l'�tat des buttons
		this.view.getNextButton().setEnabled(hasNext);
		this.view.getNextButton().repaint();
		this.view.getLastButton().setEnabled(hasPrevious);
		//Rafraichissement de la page
		this.view.getLastButton().repaint();
    }
}
