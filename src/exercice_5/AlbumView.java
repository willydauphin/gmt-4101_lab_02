package exercice_5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

public class AlbumView {
	final static int WIDTH = 1000;
	final static int HEIGHT = 800;
	final static int MAX_WIDTH_IMAGE = 800;
	final static int MAX_HEIGHT_IMAGE = 600;
    private JFrame frame;
    private JSplitPane splitPane;
    private JPanel leftPanel;
    private JPanel rightPanel;
    private JPanel infoImagePanel;
    private JPanel infoTexteImagePanel;
    private JButton modifImageButton;
    private ImagePanel imagePanel;
    private JLabel descImage;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu aboutMenu;
    private JMenuItem fileAddItem;
    private JMenuItem fileExitItem;
    private JMenuItem aboutMoreItem;
    private JDialog aboutDialog;
    private JDialog modifDialog;
    private JButton printButton;
    private JButton lastButton;
    private JButton nextButton;
    private JList<String> imagesList;
    private JFileChooser chooser; 
    private JTextField titreArea;
    private JTextArea descArea;
    private JButton validModifButton;
    private JButton closeModifButton;
	
    /**
	    Constructeur d'AlbumView
	    @param titre Titre de la fen�tre principale.
	*/
	public AlbumView(String titre) {
        //MISE EN PAGE PRINCIPALE
        this.frame = new JFrame(titre);
        this.frame.setSize(new Dimension(AlbumView.WIDTH, AlbumView.HEIGHT));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.leftPanel = new JPanel(new BorderLayout());
        this.rightPanel = new JPanel(new BorderLayout());
        this.frame.add(leftPanel, BorderLayout.CENTER);
        this.frame.add(rightPanel, BorderLayout.EAST);
        
        //MENU
		this.menuBar = new JMenuBar();
		this.fileMenu = new JMenu("Fichier");
		this.aboutMenu = new JMenu("Infos");
		this.fileAddItem = new JMenuItem("Ajouter");
		this.fileExitItem = new JMenuItem("Quitter");
		this.aboutMoreItem = new JMenuItem("A propos");
		this.fileMenu.add(this.fileAddItem);
		this.fileMenu.add(this.fileExitItem);
		this.aboutMenu.add(this.aboutMoreItem);
		this.menuBar.add(fileMenu);
		this.menuBar.add(aboutMenu);
		this.frame.setJMenuBar(this.menuBar);
        
        //INFORMATIONS
        this.aboutDialog = new JDialog(this.frame, "A propos");
        JLabel l = new JLabel("Auteurs : William & Andrey", JLabel.CENTER);
        this.aboutDialog.add(l, BorderLayout.NORTH);
        JLabel l2 = new JLabel("Description � venir...", JLabel.CENTER);
        this.aboutDialog.add(l2, BorderLayout.CENTER);
        JLabel l3 = new JLabel("Pas de copyright.", JLabel.CENTER);
        this.aboutDialog.add(l3, BorderLayout.SOUTH);
        this.aboutDialog.setSize(AlbumView.WIDTH/4, AlbumView.HEIGHT/4);
        
        //MODIF DIALOG
        this.modifDialog = new JDialog(this.frame, "Modifier informations");
        this.modifDialog.setSize(AlbumView.WIDTH/2, AlbumView.HEIGHT/3);
        JPanel panelDialog = new JPanel(new GridBagLayout());
        GridBagConstraints cs = new GridBagConstraints();
        cs.fill = GridBagConstraints.HORIZONTAL;
        JLabel lbUsername = new JLabel("Titre: ", SwingConstants.RIGHT);
        cs.gridx = 0;
        cs.gridy = 0;
        cs.gridwidth = 1;
        panelDialog.add(lbUsername, cs);
        this.titreArea = new JTextField(20);
        cs.gridx = 1;
        cs.gridy = 0;
        cs.gridwidth = 2;
        panelDialog.add(this.titreArea, cs);
        JLabel lbPassword = new JLabel("Description: ", SwingConstants.RIGHT);
        cs.gridx = 0;
        cs.gridy = 1;
        cs.gridwidth = 1;
        panelDialog.add(lbPassword, cs);
        this.descArea = new JTextArea("", 5, 30);
        cs.gridx = 1;
        cs.gridy = 1;
        cs.gridwidth = 2;
        panelDialog.add(this.descArea, cs);
        panelDialog.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.GRAY));
        JPanel bp = new JPanel();
        this.validModifButton = new JButton("Valider");
        this.closeModifButton = new JButton("Annuler");
        bp.add(this.closeModifButton);
        bp.add(this.validModifButton);
        this.modifDialog.add(panelDialog, BorderLayout.CENTER);
        this.modifDialog.add(bp, BorderLayout.PAGE_END);
        
        //LISTE
        this.imagesList = new JList<String>(new DefaultListModel<String>());
        JLabel lbl = new JLabel("Liste des images :", SwingConstants.CENTER);
        this.rightPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();  
        this.printButton = new JButton("Affiche");
        gbc.insets = new Insets(5,15,5,15);
        
        gbc.fill = GridBagConstraints.HORIZONTAL; 
        gbc.gridx = 0;  
        gbc.gridy = 0;
        this.rightPanel.add(lbl, gbc); 
        gbc.gridx = 0;  
        gbc.gridy = 1;
        gbc.ipady = AlbumView.HEIGHT/6; 
        this.rightPanel.add(this.imagesList, gbc); 
        gbc.gridx = 0;  
        gbc.gridy = 2;
        gbc.ipady = 0;
        this.rightPanel.add(this.printButton, gbc); 
        
        //PRECEDENT - SUIVANT
        this.imagePanel = new ImagePanel();
        this.imagePanel.setBorder(BorderFactory.createLineBorder(Color.black));
        this.infoImagePanel = new JPanel();
        this.infoTexteImagePanel = new JPanel();
        this.descImage = new JLabel("<html><p>Titre : </p><br><p>Description :</p></html>");
        this.infoTexteImagePanel.add(this.descImage);
        this.modifImageButton = new JButton("Modifier");
        this.infoImagePanel.add(this.infoTexteImagePanel, BorderLayout.WEST);
        this.infoImagePanel.add(this.modifImageButton, BorderLayout.EAST);
        this.infoImagePanel.setBorder(BorderFactory.createLineBorder(Color.black));
        this.leftPanel.add(this.infoImagePanel, BorderLayout.NORTH);
        this.leftPanel.add(this.imagePanel, BorderLayout.CENTER);
        JPanel buttonsPanel = new JPanel();
        this.lastButton = new JButton("Pr�c�dent");
        this.nextButton = new JButton("Suivant");
        buttonsPanel.add(this.lastButton, BorderLayout.WEST);
        buttonsPanel.add(this.nextButton, BorderLayout.EAST);
        this.leftPanel.add(buttonsPanel, BorderLayout.SOUTH);
        
        //CHOOSER
    	this.chooser = new JFileChooser();
	}
	
	//
    /**
	    Permet de lier les actions au AlbumController en respectant MVC
	    @param l1 ActionLister d'ajout de fichier.
	    @param l2 ActionLister de fermeture de la fen�tre.
	    @param l3 ActionLister d'ouverture de dialogue "� propos".
	    @param l4 ActionLister d'affichage de l'image selectionn�e.
	    @param l5 ActionLister de suivant.
	    @param l6 ActionLister de pr�c�dent.
	    @param l7 ActionLister d'ouverture fen�tre modification de l'image courante.
	    @param l8 ActionLister de fermeture fen�tre modification de l'image courante.
	    @param l9 ActionLister de validation fen�tre modification de l'image courante.
	*/
	public void addListeners(ActionListener l1, ActionListener l2, ActionListener l3, ActionListener l4, ActionListener l5, ActionListener l6, ActionListener l7, ActionListener l8, ActionListener l9){
	    this.fileAddItem.addActionListener(l1);
	    this.fileExitItem.addActionListener(l2);
	    this.aboutMoreItem.addActionListener(l3);
	    this.printButton.addActionListener(l4);
	    this.nextButton.addActionListener(l5);
	    this.lastButton.addActionListener(l6);
	    this.modifImageButton.addActionListener(l7);
	    this.closeModifButton.addActionListener(l8);
	    this.validModifButton.addActionListener(l9);
	}
	
	public JFrame getFrame() {
		return this.frame;
	}
	
	public JDialog getAboutDialog() {
		return this.aboutDialog;
	}
	
	public JDialog getModifDialog() {
		return this.modifDialog;
	}

	public JFileChooser getChooser() {
		return this.chooser;
	}

	public JList<String> getList() {
		return this.imagesList;
	}
	
	public JTextField getTitreArea() {
		return this.titreArea;
	}
	
	public JTextArea getDescArea() {
		return this.descArea;
	}

	public JButton getNextButton() {
		return this.nextButton;
	}

	public JButton getLastButton() {
		return this.lastButton;
	}
	
	/**
	    Mise � jour de l'affichage de l'image � l'�cran.
	    @param i MyImage � afficher.
	*/
	public void updateImage(MyImage i) {
		//Gestion de la taille pour le recadrage
		double width = i.getImg().getWidth(null);
		double height = i.getImg().getHeight(null);
		double maxWidth = AlbumView.MAX_WIDTH_IMAGE;
		double maxHeight = AlbumView.MAX_HEIGHT_IMAGE;
		double coef = 1;
		if(height>maxHeight || width>maxWidth ) {
			if((height-maxHeight) > (width-maxWidth)) {
				coef = maxHeight/height;
			}else {
				coef = maxWidth/width;
			}
		}
		//Chargement de l'image recadr�e (juste en vue, on ne touche pas au mod�le pour garder toutes les informations de l'image initiale) 
		this.imagePanel.setImage(ImagePanel.scale(i.getImg(), (int)(coef*width), (int)(coef*height)));
        //Changement des labels associ�s
		this.descImage.setText("<html><p>Titre : " + i.getTitre() + "</p><br><p>Description : <i>'" + i.getDescription() + "'</i></p></html>");
        this.titreArea.setText(i.getTitre());
        this.descArea.setText(i.getDescription());
        //Mise � jour de l'affichage
		this.imagePanel.repaint();
	}
}
